package com.netcracker.bulis.comparators;

import com.netcracker.bulis.entity.CreditCard;

import java.util.Map;

public class Comparators {

    public static int compareByAmountList(CreditCard c1, CreditCard c2) {
        return Long.compare(c1.getAmountOfMoney(), c2.getAmountOfMoney());
    }

    public static int compareByBankNameList(CreditCard c1, CreditCard c2) {

        return c1.getBankName().compareToIgnoreCase(c2.getBankName());
    }

    public static int compareByAmountMap(Map.Entry<Integer, CreditCard> c1, Map.Entry<Integer, CreditCard> c2) {
        return Long.compare(c1.getValue().getAmountOfMoney(), c2.getValue().getAmountOfMoney());
    }

    public static int compareByBankNameMap(Map.Entry<Integer, CreditCard> c1, Map.Entry<Integer, CreditCard> c2) {

        return c1.getValue().getBankName().compareToIgnoreCase(c2.getValue().getBankName());

    }

}
