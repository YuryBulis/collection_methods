package com.netcracker.bulis.entity;

public class CreditCard {
    private String bankName;
    private Long amountOfMoney;

    public CreditCard(){
    }

    public CreditCard(String bankName, long amountOfMoney) {
        this.bankName = bankName;
        this.amountOfMoney = amountOfMoney;
    }

    public String getBankName() {
        return bankName;
    }

    public Long getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "bankName='" + bankName + '\'' +
                ", amountOfMoney=" + amountOfMoney +
                '}';
    }
}
