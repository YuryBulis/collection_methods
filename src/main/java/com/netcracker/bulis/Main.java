package com.netcracker.bulis;

import com.netcracker.bulis.entity.CreditCard;
import com.netcracker.bulis.methods.ListCreditCardMethods;
import com.netcracker.bulis.methods.ListCreditCardStreamMethods;
import com.netcracker.bulis.methods.MapCreditCardMethods;
import com.netcracker.bulis.methods.MapCreditCardMethodsStream;

public class Main {
    public static void main(String[] args) {
        ListCreditCardMethods listCreditCardMethods = new ListCreditCardMethods();

        listCreditCardMethods.addCreditCard(new CreditCard("BelarusBank", 12000));
        listCreditCardMethods.addCreditCard(new CreditCard("PriorBank", 10000));
        listCreditCardMethods.addCreditCard(new CreditCard("GazpromBank", 15000));
        listCreditCardMethods.addCreditCard(new CreditCard("BelVebBank", 13000));

        ListCreditCardStreamMethods listCreditCardStreamMethods = new ListCreditCardStreamMethods();
        listCreditCardStreamMethods.addCreditCard(new CreditCard("BelarusBank", 12000));
        listCreditCardStreamMethods.addCreditCard(new CreditCard("PriorBank", 10000));
        listCreditCardStreamMethods.addCreditCard(new CreditCard("GazpromBank", 15000));
        listCreditCardStreamMethods.addCreditCard(new CreditCard("BelVebBank", 13000));

        MapCreditCardMethods mapCreditCardMethods = new MapCreditCardMethods();
        mapCreditCardMethods.addCreditCard(1, new CreditCard("BelarusBank", 12000));
        mapCreditCardMethods.addCreditCard(2, new CreditCard("PriorBank", 10000));
        mapCreditCardMethods.addCreditCard(3, new CreditCard("GazpromBank", 15000));
        mapCreditCardMethods.addCreditCard(4, new CreditCard("BelVebBank", 13000));

        MapCreditCardMethodsStream mapCreditCardMethodsStream = new MapCreditCardMethodsStream();
        mapCreditCardMethodsStream.addCreditCard(1, new CreditCard("BelarusBank", 12000));
        mapCreditCardMethodsStream.addCreditCard(2, new CreditCard("PriorBank", 10000));
        mapCreditCardMethodsStream.addCreditCard(3, new CreditCard("GazpromBank", 15000));
        mapCreditCardMethodsStream.addCreditCard(4, new CreditCard("BelVebBank", 13000));

        System.out.println(mapCreditCardMethods.findStringInBankName("Prio"));
    }

}
