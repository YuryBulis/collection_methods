package com.netcracker.bulis.methods;

import com.netcracker.bulis.comparators.Comparators;
import com.netcracker.bulis.entity.CreditCard;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class ListCreditCardStreamMethods {
    private ArrayList<CreditCard> creditCards;

    public ListCreditCardStreamMethods() {
        creditCards = new ArrayList<>();
    }

    public void addCreditCard(CreditCard creditCard) {
        creditCards.add(creditCard);
    }

    public ArrayList<CreditCard> deleteCardWithMaxAmountStream() {

        CreditCard card = creditCards.stream()
                .max(Comparator.comparing(CreditCard::getAmountOfMoney))
                .get();

        creditCards.remove(card);

        return creditCards;
    }

    public List<CreditCard> deleteCardWhichIsLessThanStream(Long amount) {

        return creditCards.stream()
                .filter(creditCard -> creditCard.getAmountOfMoney() > amount)
                .collect(toList());
    }

    public Long countAmountOnAllCardsStream() {

        return creditCards.stream()
                .mapToLong(CreditCard::getAmountOfMoney)
                .sum();
    }

    public CreditCard getThirdElementStream() {
        return creditCards.stream()
                .skip(2)
                .findFirst()
                .get();
    }

    public List<CreditCard> getTwoElementsBeginningFromTheSecondStream() {

        return creditCards.stream()
                .skip(1)
                .limit(2)
                .collect(Collectors.toList());
    }

    public List<CreditCard> findStringInBankNameStream(String name) {

        return creditCards.stream()
                .filter(creditCard -> creditCard.getBankName().startsWith(name))
                .collect(Collectors.toList());
    }

    public List<CreditCard> findCharInBankNameStream(Character symbol) {

        return creditCards.stream()
                .filter(creditCard -> creditCard.getBankName().contains(symbol.toString()))
                .collect(Collectors.toList());
    }

    public ArrayList<CreditCard> AddSymbolToBankNameStream() {

        creditCards.forEach(creditCard -> creditCard.setBankName(creditCard.getBankName() + "_1"));
        return creditCards;
    }

    public List<CreditCard> SortByNameAndAmountStream() {

        return creditCards.stream().
                sorted(Comparators::compareByAmountList).
                collect(Collectors.toList()).stream().
                sorted(Comparators::compareByBankNameList).
                collect(Collectors.toList());
    }


}
