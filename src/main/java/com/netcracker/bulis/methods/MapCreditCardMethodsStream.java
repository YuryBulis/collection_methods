package com.netcracker.bulis.methods;

import com.netcracker.bulis.comparators.Comparators;
import com.netcracker.bulis.entity.CreditCard;

import java.util.*;
import java.util.stream.Collectors;

public class MapCreditCardMethodsStream {
    private HashMap<Integer, CreditCard> creditCards;
    private Iterator<Map.Entry<Integer, CreditCard>> iterator;

    public MapCreditCardMethodsStream() {
        creditCards = new HashMap<>();
    }

    public void addCreditCard(Integer id, CreditCard creditCard) {
        creditCards.put(id, creditCard);
    }

    public HashMap<Integer, CreditCard> deleteCardWithMaxAmountStream() {

        Map.Entry<Integer, CreditCard> maxAmountCard = creditCards.entrySet().stream()
                .max(Comparators::compareByAmountMap)
                .orElse(null);

        creditCards.remove(maxAmountCard.getKey());
        return creditCards;
    }

    public List<Map.Entry<Integer, CreditCard>> deleteCardWhichIsLessThanStream(Long amount) {

        return creditCards.entrySet().stream()
                .filter(card -> card.getValue().getAmountOfMoney() > amount)
                .collect(Collectors.toList());
    }

    public Long countAmountOnAllCardsStream() {

        return creditCards.entrySet().stream()
                .mapToLong(card -> card.getValue().getAmountOfMoney())
                .sum();
    }

    public Map.Entry<Integer, CreditCard> getThirdElementStream() {
        return creditCards.entrySet()
                .stream()
                .skip(2)
                .findFirst()
                .get();
    }

    public List<Map.Entry<Integer, CreditCard>> getTwoElementsBeginningFromTheSecondStream() {
        return creditCards.entrySet()
                .stream()
                .skip(1)
                .limit(2)
                .collect(Collectors.toList());
    }

    public List<Map.Entry<Integer, CreditCard>> findStringInBankNameStream(String name) {
        return creditCards.entrySet()
                .stream()
                .filter(creditCard -> creditCard.getValue().getBankName().startsWith(name))
                .collect(Collectors.toList());
    }

    public List<Map.Entry<Integer, CreditCard>> findCharInBankNameStream(Character symbol) {
        return creditCards.entrySet()
                .stream()
                .filter(creditCard -> creditCard.getValue().getBankName().contains(symbol.toString()))
                .collect(Collectors.toList());
    }

    public HashMap<Integer, CreditCard> AddSymbolToBankNameStream() {
        creditCards.entrySet()
                .forEach((creditCard -> creditCard.getValue().setBankName(creditCard.getValue().getBankName() + "_1")));
        return creditCards;
    }

    public List<Map.Entry<Integer, CreditCard>> sortByUsernameAndSubsStream() {
        return creditCards.entrySet()
                .stream().
                        sorted(Comparators::compareByAmountMap).
                        collect(Collectors.toList()).stream().
                        sorted(Comparators::compareByBankNameMap).
                        collect(Collectors.toList());
    }
}
