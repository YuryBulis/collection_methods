package com.netcracker.bulis.methods;

import com.netcracker.bulis.comparators.Comparators;
import com.netcracker.bulis.entity.CreditCard;

import java.util.*;

public class ListCreditCardMethods {
    private ArrayList<CreditCard> creditCards;
    private Iterator<CreditCard> iterator;

    public ListCreditCardMethods() {
        creditCards = new ArrayList<>();
    }

    public void addCreditCard(CreditCard creditCard) {
        creditCards.add(creditCard);
    }

    public ArrayList<CreditCard> deleteCardWithMaxAmount() {
        long maxAmount = creditCards.get(0).getAmountOfMoney();
        for (CreditCard c : creditCards) {
            if (c.getAmountOfMoney() > maxAmount) {
                maxAmount = c.getAmountOfMoney();
            }
        }

        for (int i = 0; i < creditCards.size(); i++) {
            if (creditCards.get(i).getAmountOfMoney() == maxAmount) {
                creditCards.remove(i);
            }
        }

        return creditCards;
    }

    public ArrayList<CreditCard> deleteCardWhichIsLessThan(Long amount) {
        iterator = creditCards.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getAmountOfMoney() < amount) {
                iterator.remove();
            }
        }
        return creditCards;
    }

    public Long countAmountOnAllCards() {
        long amount = 0;
        iterator = creditCards.iterator();
        while (iterator.hasNext()) {
            amount += iterator.next().getAmountOfMoney();
        }
        return amount;
    }

    public CreditCard getThirdElement() {
        return creditCards.get(2);
    }

    public List<CreditCard> getTwoElementsBeginningFromTheSecond() {
        return creditCards.subList(1, 3);
    }

    public ArrayList<CreditCard> findStringInBankName(String name) {
        ArrayList<CreditCard> subCreditCard = new ArrayList<>();
        for (CreditCard c : creditCards) {
            if (c.getBankName().startsWith(name)) {
                subCreditCard.add(c);
            }
        }
        return subCreditCard;
    }

    public ArrayList<CreditCard> findCharInBankName(Character symbol) {
        ArrayList<CreditCard> subCreditCard = new ArrayList<>();
        for (CreditCard c : creditCards) {
            if (c.getBankName().contains(symbol.toString())) {
                subCreditCard.add(c);
            }
        }
        return subCreditCard;
    }

    public ArrayList<CreditCard> AddSymbolToBankName() {
        for (CreditCard c : creditCards) {
            c.setBankName(c.getBankName().concat("_1"));
        }
        return creditCards;
    }

    public ArrayList<CreditCard> SortByNameAndAmount() {

        creditCards.sort(Comparators::compareByAmountList);
        creditCards.sort(Comparators::compareByBankNameList);
        return creditCards;
    }

}

