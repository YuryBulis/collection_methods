package com.netcracker.bulis.methods;

import com.netcracker.bulis.comparators.Comparators;
import com.netcracker.bulis.entity.CreditCard;

import java.util.*;

public class MapCreditCardMethods {
    private HashMap<Integer, CreditCard> creditCards;
    private Iterator<Map.Entry<Integer, CreditCard>> iterator;

    public MapCreditCardMethods() {
        creditCards = new HashMap<>();
    }

    public void addCreditCard(Integer id, CreditCard creditCard) {
        creditCards.put(id, creditCard);
    }

    public HashMap<Integer, CreditCard> deleteCardWithMaxAmount() {
        Map.Entry<Integer, CreditCard> maxAmountCard = creditCards.entrySet().iterator().next();
        for (Map.Entry<Integer, CreditCard> c : creditCards.entrySet()) {
            if (c.getValue().getAmountOfMoney() > maxAmountCard.getValue().getAmountOfMoney()) {
                maxAmountCard = c;
            }
        }
        creditCards.remove(maxAmountCard.getKey());
        return creditCards;
    }

    public HashMap<Integer, CreditCard> deleteCardWhichIsLessThan(Long amount) {
        iterator = creditCards.entrySet().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getValue().getAmountOfMoney() < amount) {
                iterator.remove();
            }
        }
        return creditCards;
    }

    public Long countAmountOnAllCards() {
        long amount = 0;
        iterator = creditCards.entrySet().iterator();
        while (iterator.hasNext()) {
            amount += iterator.next().getValue().getAmountOfMoney();
        }
        return amount;
    }

    public CreditCard getThirdElement() {
        return creditCards.get(3);
    }

    public List<CreditCard> getTwoElementsBeginningFromTheSecond() {
        List<CreditCard> card = new ArrayList<>();
        card.add(creditCards.get(2));
        card.add(creditCards.get(3));
        return card;
    }

    public HashMap<Integer, CreditCard> findStringInBankName(String name) {
        HashMap<Integer, CreditCard> subCreditCard = new HashMap<>();
        for (Integer key : creditCards.keySet()) {
            if (creditCards.get(key).getBankName().contains(name)) {
                subCreditCard.put(key, creditCards.get(key));
            }
        }
        return subCreditCard;
    }

    public HashMap<Integer, CreditCard> findCharInBankName(Character symbol) {
        HashMap<Integer, CreditCard> subCreditCard = new HashMap<>();
        for (Integer key : creditCards.keySet()) {
            if (creditCards.get(key).getBankName().contains(symbol.toString())) {
                subCreditCard.put(key, creditCards.get(key));
            }
        }
        return subCreditCard;
    }

    public HashMap<Integer, CreditCard> AddSymbolToBankName() {
        for (Map.Entry<Integer, CreditCard> c : creditCards.entrySet()) {
            c.getValue().setBankName(c.getValue().getBankName().concat("_1"));
        }
        return creditCards;
    }

    public List<Map.Entry<Integer, CreditCard>> sortByUsernameAndSubs() {
        List<Map.Entry<Integer, CreditCard>> card = new ArrayList<>(creditCards.entrySet());
        card.sort(Comparators::compareByAmountMap);
        card.sort(Comparators::compareByBankNameMap);
        return card;
    }


}
